import java.sql.*;


public class AudioPlayer extends player {

    public void getPlayer() {
        try {
            Connection c = ConnectToDataBase.dbconn();
            PreparedStatement stmt = c.prepareStatement("SELECT PlayerName FROM playerdetails where FileType='Audio'");
            ResultSet rs;
            rs = stmt.executeQuery();
            System.out.println("Entered file is an Audio file and it's content can be played in Office.\nBest Players to play audio files are:\n");
            while (rs.next())
                System.out.println(rs.getString(1));

        } catch (SQLException e) {
            System.out.println("Could not connect to DataBase");
        }
    }
}


