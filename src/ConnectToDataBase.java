import java.sql.Connection;
import java.sql.DriverManager;
public class ConnectToDataBase{

    public static Connection dbconn()
    {
        Connection connection = null;
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/players?autoReconnect=true&useSSL=false",
                    "root","qwertyuiop");
            return (connection);

        }
        catch (Exception e)
        {
            return(connection);
        }

    }
}

