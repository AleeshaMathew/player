import java.sql.*;

public class FileExtension {
    Connection c = null;

    public void checkFileName(String nameOfFile) {
        AudioPlayer audioPlayer = new AudioPlayer();
        VideoPlayer videoPlayer = new VideoPlayer();


        String fileName;
        fileName = nameOfFile.substring(nameOfFile.lastIndexOf(".") + 1);
        try {
            c = ConnectToDataBase.dbconn();
            PreparedStatement stmt = c.prepareStatement("SELECT FileType FROM fileextension where Extension=?");
            stmt.setString(1, fileName);
            ResultSet rs;
            rs = stmt.executeQuery();

            while (rs.next()) {

                if ((rs.getString(1)).equals("Audio")) {
                    audioPlayer.getPlayer();
                } else if ((rs.getString(1)).equals("Video")) {
                    videoPlayer.getPlayer();
                }
            }
        } catch (SQLException e) {
            System.out.println("Could not connect to DataBase");
        } finally {
            try {
                c.close();
               
            } catch (SQLException e) {
                System.out.println("Could not connect to DataBase");
            }

        }
    }

}










