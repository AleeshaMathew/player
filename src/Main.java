import java.util.Scanner;

public class Main {

    public static void main(String args[]) {

        FileExtension fileExtension= new FileExtension();

        System.out.println("Enter the file name with extension\n(For example: musicfilename.mp3 or videofilename.mp4)\n");
        String filename;
        Scanner fileName = new Scanner(System.in);
        filename = fileName.next();
        fileExtension.checkFileName(filename);

    }
}
